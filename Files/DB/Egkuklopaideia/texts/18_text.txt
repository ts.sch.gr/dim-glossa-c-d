txt=Οι θαλάσσιες χελώνες έζησαν την ίδια εποχή με τους δεινόσαυρους. Αν και οι δεινόσαυροι εξαφανίστηκαν, οι χελώνες επιβιώνουν μέχρι σήμερα. Οι αρχαίες χελώνες ζούσαν σε έλη. Αργότερα μερικές απ' αυτές άρχισαν να ζουν στη στεριά ενώ άλλες περνούσαν το μεγαλύτερο μέρος της ζωής τους στο νερό. Έχουν πνεύμονες και αναπνέουν αέρα. Γεννούν τα αυγά τους στη στεριά. Το σώμα τους προστατεύεται σε ένα καβούκι.
Οι θαλάσσιες χελώνες ζουν στη θάλασσα. Εκεί ζευγαρώνουν, τρέφονται και περνούν το χειμώνα. Οι θηλυκές επιστρέφουν στην ακτή για να σκάψουν φωλιές και να γεννήσουν τα αυγά τους. Οι αρσενικές δε γυρνούν σχεδόν ποτέ στη στεριά. Τα μικρά χελωνάκια, όταν βγουν από τα αυγά τους, πάνε προς τη θάλασσα. <br>
<br>Τροφικέςσυνήθειες
Οι θαλάσσιες χελώνες τρέφονται με αργοκίνητα ή ακίνητα ζώα, όπως: οστρακοειδή, τσούχτρες, μαλάκια, αχινούς, καβούρια, σφουγγάρια, αλλά και με θαλάσσια φυτά ή φύκια  <br>
<br>Φυσικές απειλές: 
Ο άνεμος, η βροχή και το κρύο καθώς και οι πολύ υψηλές θερμοκρασίες επηρεάζουν τις χελώνες σε όλα τα στάδια της ζωής τους. Στην Ελλάδα αλεπούδες, σκυλιά και καμιά φορά τσακάλια μπορεί να σκάψουν για τα αυγά. Τα ίδια ζώα τρώνε καμιά φορά και τους νεοσσούς που τρέχουν προς τη θάλασσα. Το ίδιο κάνουν και κουνάβια, αρουραίοι ή πουλιά, όπως κοράκια, γλάροι και κορμοράνοι. Από τη στιγμή που οι νεοσσοί φθάσουν τη θάλασσα, κινδυνεύουν από τα μεγάλα ψάρια. <br>
<br>Απειλές από τον άνθρωπο
Οι άνθρωποι δυστυχώς: 
1.	Τις πιάνουν για να εκμεταλλευτούν το κρέας και μέρη του σώματος.
2.	Μαζεύουν τα αυγά για κατανάλωση
3.	Οι χελώνες πολλές φορές μπλέκονται στα δίχτυα και πνίγονται ή θανατώνονται από τους ψαράδες 
4.	Οι παραλίες, όπου γεννούν τα αυγά τους, λιγοστεύουν. Σπίτια, καταστήματα και ξενοδοχεία κτίζονται παντού. Τουρίστες γεμίζουν τις ακτές.
5.	Τα φώτα που λάμπουν στις παραλίες μπερδεύουν και τις θηλυκές, όταν βγαίνουν να γεννήσουν, αλλά και τα μικρά που προσπαθούν να φθάσουν στη θάλασσα.
6.	Τα αυτοκίνητα που κυκλοφορούν πάνω στην άμμο την πιέζουν και έτσι δεν κυκλοφορεί σωστά ο αέρας και τα αυγά δεν μπορούν να τον απορροφήσουν.  
7.	Οι ομπρέλες και οι ξαπλώστρες στις παραλίες, όπου γεννούν τα αυγά οι χελώνες, συχνά τις εμποδίζουν να περάσουν στο πίσω μέρος της παραλίας για να γεννήσουν.
8.	Τα δέντρα που φυτεύονται και οι ομπρέλες που στήνονται στις παραλίες ρίχνουν σκιά στις φωλιές, η θερμοκρασία είναι πιο χαμηλή και τα αυγά δεν επωάζονται σωστά. 
9.	Οι άνθρωποι που διασκεδάζουν στις παραλίες τη νύχτα τρομάζουν τις θαλάσσιες χελώνες που θέλουν να γεννήσουν
10.	Τα κάστρα στην άμμο ή οι ροδιές από τροχοφόρα μπορεί να παγιδέψουν τους νεοσσούς που προχωράνε προς τη θάλασσα.
11.	Η ρύπανση των θαλασσών είναι μια ακόμα απειλή. Οι χελώνες συχνά μπερδεύουν πεταμένες πλαστικές σακούλες με τσούχτρες και μπάλες πίσσας με κάτι φαγώσιμο. Αν τα καταπιούν μπορεί να πεθάνουν.
<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Εισαγωγή Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL INSERT RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'INSERT INTO kryptolekso (puzzle_id, teacher_id, data_text, image) VALUES ('. $_POST['puzzle'] .', '.$_SESSION['teacher_id'].', "'.trim($_POST['data_text']).'", "'.$_POST['image'].'")';

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


echo '	<form id="mc" name="mc" method="post" action="insert_kryptolekso.php">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._INSERTKRUPTO.'</td>
		</tr>
		</table>';

echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._PUZZLE.':</td>
			<td class="td2" width="255"><input id="puzzle" name="puzzle" style="width:25px;" maxlength="3"></td>
			<td class="td8">'._PUZZLEHELP.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._WORDS.':</td>
			<td class="td2" width="255"><textarea id="data_text" name="data_text" style="width:250px;" rows="7" onKeyUp="checkTextAreaWordLength(\'data_text\', \'\n\', 10, \''._KRUPTOMAXWORDS.'\');checkTextAreaLines(\'data_text\', 15, \''._KRUPTOMSG.'\')"></textarea></td>
			<td class="td8">'._WORDSDESC4.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._IMAGE.':</td>
			<td class="td2" width="255">'.getImageForEI('','').'</td>
			<td class="td8"><a href="javascript:;" onclick="'.uploadPhotoLink('kryptolekso').'">'._INSERTIMAGE.'</a></td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>
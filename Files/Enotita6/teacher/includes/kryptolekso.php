<?php

if (!$_SESSION['isUser']) unauthorized();

//CHANGE THE PUZZLE
if ($_POST['changePuzzle']) {
	if ($_POST['choosePuzzle'] == "none") {

		$sql = 'UPDATE '.$_POST['activity_table'].' SET viewable = \'no\' WHERE teacher_id = '.$_SESSION['teacher_id'];
		$rs =& $dbconn->Execute($sql);
	
		if (!$rs) echo $dbconn->ErrorMsg();
	}
	else {

		$sql = 'UPDATE '.$_POST['activity_table'].' SET viewable = \'no\' WHERE teacher_id = '.$_SESSION['teacher_id'];
		$rs =& $dbconn->Execute($sql);
	
		if (!$rs) echo $dbconn->ErrorMsg();

		$sql = 'UPDATE '.$_POST['activity_table'].' SET viewable = \'yes\' WHERE teacher_id = '.$_SESSION['teacher_id'].' AND puzzle_id = '.$_POST['choosePuzzle'];
		$rs =& $dbconn->Execute($sql);
	
		if (!$rs) echo $dbconn->ErrorMsg();
	}
}

// DELETE
if ($_POST['delete'])
{
	$sql = 'DELETE FROM '.$_POST['activity_table'].' WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_POST['delete'];
	$rs =& $dbconn->Execute($sql);
	
	if (!$rs) echo $dbconn->ErrorMsg();
}

// GET DATA  ///////////////////////

// number of records to fetch
$limit = 5;

// start -> beginning of records
if (empty($_POST['start']))
{
	$_POST['start'] = 0;
}

// order -> order of records
if (empty($_POST['order']))
{
	$_POST['order'] = 'id';
}


// get records with limit
$sql = 'SELECT
			id,
			puzzle_id,
			teacher_id, 
			data_text, 
			image
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id'].'
		ORDER BY '.$_POST['order'].' DESC
		LIMIT '.$_POST['start'].', '.$limit;
$result =& $dbconn->Execute($sql);
$rs =& $dbconn->Execute($sql);
$num_of_records = $rs->RecordCount();


// get records without limit
$tsql = 'SELECT count(*)
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id'];
$trs = &$dbconn->Execute($tsql);
$total_records = $trs->fields[0];

/////////////////////////////////////////////////////////////


echo '	<form id="mu" name="mu" method="post" action="main.php">';

echo '
<table width="80%" border="0" cellpadding="4" cellspacing="2">
<tr>
	<td class="td1">'._PUZZLE.'</td>
	<td class="td1">'._WORDS.'</td>
	<td class="td1" align="center">'._IMAGE.'</td>
	<td class="td1" colspan="2" align="center">'._ACTIONS.'</td>
</tr>';

while (!$result->EOF)
{	
	$line = 'line_'.$result->fields[0];
	
	echo '
	<tr id="'.$line.'" bgcolor="#FFFFFF" onMouseOver="changeColor(\''.$line.'\',\''.$color1.'\');" onMouseOut="changeColor(\''.$line.'\',\''.$color2.'\');">
		<td class="td2" align="center">'.$result->fields[1].'</td>
		<td class="td2">'.str_replace("\r\n","<br>",$result->fields[3]).'</td>
		<td class="td2" align="center">'.getImage($result->fields[4],$_POST['activity_table']).'</td>
		<td class="td2" align="center"><a href="javascript:;" onClick="popModal(\'includes/edit_'.$_POST['activity_table'].'.php?id='.$result->fields[0].'\',600,440);" title="'._EDIT.'">'._EDIT.'</a></td>
		<td class="td2" align="center"><a href="javascript:confirmDelete('.$result->fields[0].');" title="'._DELETE.'">'._DELETE.'</a></td>
	</tr>';

	$result->MoveNext();
}

echo '
</table>';


echo '<br>';


//Find all the distinct puzzles
// get records with limit
$dsql = 'SELECT DISTINCT 
			puzzle_id
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id'].' ORDER BY puzzle_id';

$dres =& $dbconn->Execute($dsql);
$numOfPuzzles = $dres->RecordCount();

//Find puzzle that is currently active
$vsql = 'SELECT puzzle_id
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id']. ' AND viewable = \'yes\' GROUP BY puzzle_id'; 

$vres =& $dbconn->Execute($vsql);
$isAnyActive = $dres->RecordCount();


if (!$vres->EOF) {
	$whoIsActive = $vres->fields[0];
	$firstItemToCheck = "";
}
else {
	$whoIsActive = -1;
	$firstItemToCheck = "checked";
}


if ($numOfPuzzles > 0)
{
	echo '<table width="80%" border="0" cellspacing="2" cellpadding="4">
			<tr> 
				<td class="td1" align="center">'._CHOOSEANDSHOW.'</td>
			</tr>
			<tr>
				<td align="center">
				<input type = "radio" id="choosePuzzle" name="choosePuzzle" value="none" onClick="changeState(\'changePuzzle\')" '. $firstItemToCheck .'>'._NONE.'&nbsp;';
					while (!$dres->EOF) {
						$puzzle = $dres->fields[0];

						if ($puzzle == $whoIsActive)
							$isChecked = "checked";
						else
							$isChecked = "";

						echo '<input type = "radio" value="'.$puzzle.'" id="choosePuzzle" name="choosePuzzle" onClick="changeState(\'changePuzzle\')" '. $isChecked .'>'.$puzzle;
						$dres->MoveNext();
					}
	echo '			<br><input type="submit" value="'._SAVE.'">
				</td>
			</tr>
		</table>';

}




echo '	<table width="80%" border="0" cellspacing="2" cellpadding="4">';

if ($num_of_records > 0)
{
	echo '
		<tr> 
			<td class="td3">'.numberOfResults($_POST['start'],$num_of_records,$total_records).'</td>
		</tr>';
}

if ($total_records > $limit)
{
	echo '
		<tr> 
			<td class="td4">'.navigator('mu',$_POST['start'],$num_of_records,$total_records,$limit).'</td>
		</tr>';
}

echo '	<tr> 
			<td class="td5"><a href="javascript:;" onClick="changeState(\'changePuzzle\');popModal(\'includes/insert_'.$_POST['activity_table'].'.php\',600,440);">'._NEWRECORD.'</a></td>
		</tr>
		</table>';

echo '	<input type="hidden" id="changePuzzle" name="changePuzzle" value="0">';
echo '	<input type="hidden" id="start" name="start" value="'.$_POST['start'].'">';
echo '	<input type="hidden" id="order" name="order" value="'.$_POST['order'].'">';
echo '	<input type="hidden" id="activity_table" name="activity_table" value="'.$_POST['activity_table'].'">';
echo '	<input type="hidden" id="delete" name="delete" value="0">';

echo '	</form>';


?>
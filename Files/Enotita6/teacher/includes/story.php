<?php

if (!$_SESSION['isUser']) unauthorized();

//CHANGE THE STORY
if ($_POST['changeStory']) 
{
	if ($_POST['chooseStory'] == "none") 
	{

		$sql = 'UPDATE '.$_POST['activity_table'].' SET viewable = "no" WHERE teacher_id = '.$_SESSION['teacher_id'];
		$rs =& $dbconn->Execute($sql);
	
		if (!$rs) echo $dbconn->ErrorMsg();
	}
	else 
	{
		$sql = 'UPDATE '.$_POST['activity_table'].' SET viewable = "no" WHERE teacher_id = '.$_SESSION['teacher_id'];
		$rs =& $dbconn->Execute($sql);
	
		if (!$rs) echo $dbconn->ErrorMsg();

		$sql = 'UPDATE '.$_POST['activity_table'].' SET viewable = "yes" WHERE teacher_id = '.$_SESSION['teacher_id'].' AND story_id = '.$_POST['chooseStory'];
		$rs =& $dbconn->Execute($sql);
	
		if (!$rs) echo $dbconn->ErrorMsg();
	}
}

// DELETE
if ($_POST['delete'])
{
	$sql = 'DELETE FROM '.$_POST['activity_table'].' WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_POST['delete'];
	$rs =& $dbconn->Execute($sql);
	
	if (!$rs) echo $dbconn->ErrorMsg();
}

// GET DATA  ///////////////////////

// number of records to fetch
$limit = 5;

// start -> beginning of records
if (empty($_POST['start']))
{
	$_POST['start'] = 0;
}

// order -> order of records
if (empty($_POST['order']))
{
	$_POST['order'] = 'id';
}


// get records with limit
$sql = 'SELECT
			id,
			teacher_id, 
			story_id,
			text, 
			image,
			sound,
			rank
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id'].'
		ORDER BY story_id, rank ASC
		LIMIT '.$_POST['start'].', '.$limit;
$result =& $dbconn->Execute($sql);
$rs =& $dbconn->Execute($sql);
$num_of_records = $rs->RecordCount();


// get records without limit
$tsql ='SELECT count(*)
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id'];

$trs = &$dbconn->Execute($tsql);
$total_records = $trs->fields[0];

/////////////////////////////////////////////////////////////


echo '	<form id="mu" name="mu" method="post" action="main.php">';

echo '
<table width="80%" border="0" cellpadding="4" cellspacing="2">
<tr>
	<td class="td1">'._STORY.'</td>
	<td class="td1">'._PARAGRAPH.'</td>
	<td class="td1">'._IMAGE.'</td>
	<td class="td1">'._SOUND.'</td>
	<td class="td1">'._RANK.'</td>
	<td class="td1" colspan="2" align="center">'._ACTIONS.'</td>
</tr>';

$stories = Array();

while (!$result->EOF)
{	
	if (!in_array($result->fields[2], $stories))
		array_push($stories, $result->fields[2]);
	
	$line = 'line_'.$result->fields[0];
	
	echo '
	<tr id="'.$line.'" bgcolor="#FFFFFF" onMouseOver="changeColor(\''.$line.'\',\''.$color1.'\');" onMouseOut="changeColor(\''.$line.'\',\''.$color2.'\');">
		<td class="td2">'.$result->fields[2].'</td>
		<td class="td2">'.str_replace("\r\n","<br>",$result->fields[3]).'</td>
		<td class="td2" align="center">'.getImage($result->fields[4],$_POST['activity_table']).'</td>
		<td class="td2" align="center">'.getSound($result->fields[5],$_POST['activity_table']).'</td>
		<td class="td2">'.$result->fields[6].'</td>
		<td class="td2" align="center"><a href="javascript:;" onClick="popModal(\'includes/edit_'.$_POST['activity_table'].'.php?id='.$result->fields[0].'\',600,440);" title="'._EDIT.'">'._EDIT.'</a></td>
		<td class="td2" align="center"><a href="javascript:confirmDelete('.$result->fields[0].');" title="'._DELETE.'">'._DELETE.'</a></td>
	</tr>';

	$result->MoveNext();
}

echo '
</table>';


echo '<br>';

// Find all the distinct stories
// get records with limit
$dsql = '	SELECT DISTINCT story_id
			FROM '.$_POST['activity_table'].'
			WHERE teacher_id = '.$_SESSION['teacher_id'].' ORDER BY story_id';

$dres =& $dbconn->Execute($dsql);
$numOfStories = $dres->RecordCount();

// Find story that is currently active
$vsql = 'SELECT story_id
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id']. ' AND viewable = \'yes\' GROUP BY story_id'; 

$vres =& $dbconn->Execute($vsql);
$isAnyActive = $dres->RecordCount();


if (!$vres->EOF) 
{
	$whoIsActive = $vres->fields[0];
	$firstItemToCheck = "";
}
else 
{
	$whoIsActive = -1;
	$firstItemToCheck = "checked";
}


if ($numOfStories > 0)
{
	echo '<table width="80%" border="0" cellspacing="2" cellpadding="4">
			<tr> 
				<td class="td1" align="center">'._CHOOSEANDSHOW.'</td>
			</tr>
			<tr>
				<td align="center">
				<input type = "radio" id="chooseStory" name="chooseStory" value="none" onClick="changeState(\'changeStory\')" '. $firstItemToCheck .'>'._NONE.'&nbsp;';
				
				while (!$dres->EOF) 
				{
					$story = $dres->fields[0];

					if ($story == $whoIsActive)
					{
						$isChecked = "checked";
					}
					else
					{
						$isChecked = "";
					}

					echo '<input type = "radio" value="'.$story.'" id="chooseStory" name="chooseStory" onClick="changeState(\'changeStory\')" '. $isChecked .'>'.$story;
					
					$dres->MoveNext();
				}
	echo '		
				<br>
				<input type="submit" value="'._SAVE.'">
				</td>
			</tr>
		</table>';

}

echo '	<table width="80%" border="0" cellspacing="2" cellpadding="4">';

if ($num_of_records > 0)
{
	echo '
		<tr> 
			<td class="td3">'.numberOfResults($_POST['start'],$num_of_records,$total_records).'</td>
		</tr>';
}

if ($total_records > $limit)
{
	echo '
		<tr> 
			<td class="td4">'.navigator('mu',$_POST['start'],$num_of_records,$total_records,$limit).'</td>
		</tr>';
}

echo '	<tr> 
			<td class="td5"><a href="javascript:;" onClick="changeState(\'changeStory\');popModal(\'includes/insert_'.$_POST['activity_table'].'.php\',600,440);">'._NEWRECORD.'</a></td>
		</tr>
		</table>';

echo '	<input type="hidden" id="changeStory" name="changeStory" value="0">';
echo '	<input type="hidden" id="start" name="start" value="'.$_POST['start'].'">';
echo '	<input type="hidden" id="order" name="order" value="'.$_POST['order'].'">';
echo '	<input type="hidden" id="activity_table" name="activity_table" value="'.$_POST['activity_table'].'">';
echo '	<input type="hidden" id="delete" name="delete" value="0">';

echo '	</form>';

?>
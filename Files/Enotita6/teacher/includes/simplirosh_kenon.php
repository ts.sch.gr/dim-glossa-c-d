<?php

if (!$_SESSION['isUser']) unauthorized();

// DELETE
if ($_POST['delete'])
{
	$sql = 'DELETE FROM '.$_POST['activity_table'].' WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_POST['delete'];
	$rs =& $dbconn->Execute($sql);
	
	if (!$rs) echo $dbconn->ErrorMsg();
}

// GET DATA ///////////////////////

// number of records to fetch
$limit = 20;

// start -> beginning of records
if (empty($_POST['start']))
{
	$_POST['start'] = 0;
}

// order -> order of records
if (empty($_POST['order']))
{
	$_POST['order'] = 'word_without';
}


// get records with limit
$sql = 'SELECT
			id,
			teacher_id, 
			word_without, 
			syllables,
			show_record
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id'].'
		ORDER BY '.$_POST['order'].' ASC
		LIMIT '.$_POST['start'].', '.$limit;
$result =& $dbconn->Execute($sql);
$rs =& $dbconn->Execute($sql);
$num_of_records = $rs->RecordCount();


// get records without limit
$tsql = 'SELECT count(*)
		FROM '.$_POST['activity_table'].'
		WHERE teacher_id = '.$_SESSION['teacher_id'];
$trs = &$dbconn->Execute($tsql);
$total_records = $trs->fields[0];

/////////////////////////////////////////////////////////////


echo '	<form id="mu" name="mu" method="post" action="main.php">';

echo '
<table width="80%" border="0" cellpadding="4" cellspacing="2">
<tr>
	<td class="td1">'._KEIMENOWITHOUTSYLLABLES.'</td>
	<td class="td1">'._SYLLABLESWORDS.'</td>
	<td class="td1" align="center">'._SHOWINACTIVITY.'</td>
	<td class="td1" colspan="2" align="center">'._ACTIONS.'</td>
</tr>';

while (!$result->EOF)
{	
	$line = 'line_'.$result->fields[0];

	echo '
	<tr id="'.$line.'" bgcolor="#FFFFFF" onMouseOver="changeColor(\''.$line.'\',\''.$color1.'\');" onMouseOut="changeColor(\''.$line.'\',\''.$color2.'\');">
		<td class="td2">'.$result->fields[2].'</td>
		<td class="td2">'.$result->fields[3].'</td>
		<td class="td2" align="center">'.showInActivity($result->fields[4],'disabled').'</td>
		<td class="td2" align="center"><a href="javascript:;" onClick="popModal(\'includes/edit_'.$_POST['activity_table'].'.php?id='.$result->fields[0].'\',600,400);" title="'._EDIT.'">'._EDIT.'</a></td>
		<td class="td2" align="center"><a href="javascript:confirmDelete('.$result->fields[0].');" title="'._DELETE.'">'._DELETE.'</a></td>
	</tr>';

	$result->MoveNext();
}

echo '
</table>';


echo '<br>';


echo '	<table width="80%" border="0" cellspacing="2" cellpadding="4">';

if ($num_of_records > 0)
{
	echo '
		<tr> 
			<td class="td3">'.numberOfResults($_POST['start'],$num_of_records,$total_records).'</td>
		</tr>';
}

if ($total_records > $limit)
{
	echo '
		<tr> 
			<td class="td4">'.navigator('mu',$_POST['start'],$num_of_records,$total_records,$limit).'</td>
		</tr>';
}

echo '	<tr> 
			<td class="td5"><a href="javascript:;" onClick="popModal(\'includes/insert_'.$_POST['activity_table'].'.php\',600,400);">'._NEWRECORD.'</a></td>
		</tr>
		</table>';

echo '	<input type="hidden" id="start" name="start" value="'.$_POST['start'].'">';
echo '	<input type="hidden" id="order" name="order" value="'.$_POST['order'].'">';
echo '	<input type="hidden" id="activity_table" name="activity_table" value="'.$_POST['activity_table'].'">';
echo '	<input type="hidden" id="delete" name="delete" value="0">';

echo '	</form>';


?>

<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Επεξεργασία Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL UPDATE RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'UPDATE magikes_eikones SET text = "'.$_POST['text'].'", words_before = "'.$_POST['words_before'].'", words_after = "'.$_POST['words_after'].'", images = "'.trim($_POST['data_images']).'", viewable = '.$_POST['show_record'].' WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';

}


// SQL GET RECORDS /////////////////////////

$sql = 'SELECT text, words_before, words_after, images, viewable FROM magikes_eikones WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];
$rs =& $dbconn->GetRow($sql);
////////////////////////////////////////////


echo '	<form id="mc" name="mc" method="post" action="edit_magikes_eikones.php?id='.$_GET['id'].'">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._EDITMAGIKESEIKONES.'</td>
		</tr>
		</table>';


if (empty($rs['images']))
{
	$multiple_images_link = '<a href="javascript:;" onclick="openWindow(\'uploadMultiplePhotos.php?fld=magikes_eikones\',250,310);">'._INSERTIMAGES.'</a>';
}
else
{
	$multiple_images_link = '<a href="javascript:;" onclick="openWindow(\'uploadMultiplePhotos.php?fld=magikes_eikones\',250,310);">'._CHANGEIMAGES.'</a><br><a href="javascript:;" onclick="deleteMultipleImages();">'._DELETEIMAGES.'</a>';
}


echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._TEXT.':</td>
			<td class="td2" width="255"><textarea id="text" name="text" style="width:270px;" rows="9">'.$rs['text'].'</textarea></td>
			<td class="td8">'._KEIMENODESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._TEXTBEFORE.':</td>
			<td class="td2" width="255"><input type="text" id="words_before" name="words_before" style="width:270px;" value="'.$rs['words_before'].'"></td>
			<td class="td8">'._WORDSBEFOREDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._TEXTAFTER.':</td>
			<td class="td2" width="255"><input id="words_after" name="words_after" style="width:270px;" value = "'.$rs['words_after'].'"></td>
			<td class="td8">'._WORDSAFTERDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._IMAGES.':</td>
			<td class="td2" width="255">
			<textarea id="tmp" name="tmp" style="width:250px;" rows="7" disabled>'.$rs['images'].'</textarea>
			<input type="hidden" id="data_images" name="data_images" value="'.$rs['images'].'">
			</td>
			<td class="td8">'.$multiple_images_link.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._SHOWINACTIVITY.':</td>
			<td class="td2" width="255">'.showInActivity($rs['viewable'],'').'</td>
			<td class="td8">'._SHOWINACTIVITYDESC.'</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>
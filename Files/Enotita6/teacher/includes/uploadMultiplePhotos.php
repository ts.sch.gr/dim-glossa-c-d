<?php

session_start();
	
header('Content-Type: text/html; charset=utf-8');

include "../API.php";

include "../language/ell.php";

if (!$_SESSION['isUser']) unauthorized();


$server_path = $_SERVER['SCRIPT_FILENAME'];
$photos_path = substr($server_path,0,strpos($server_path,'teacher')).'media/images/'.$_GET['fld'].'/';


if ($_POST['action']) 
{ 	
	$error = '';
	$filename = '';

	?>

	<SCRIPT>
	window.opener.document.getElementById('tmp').value = "";
	window.opener.document.getElementById('data_images').value = "";
	</SCRIPT>

	<?php

	// Άλλαξε τα δικαιώματα σε 777 (απο 755) για να μπορέσουμε να αποθηκεύσουμε το αρχείο
	chmod($photos_path, 0777);
	
	for ($i=0 ; $i < $_POST['counter'] ; $i++)
	{
		if (!empty($_FILES['images']['name'][$i]))
		{
			$filename = $_FILES['images']['name'][$i];

			// Error Number Given By PHP
			$error_number = $_FILES['images']['error'][$i];
			
			// Όνομα αρχείου
			$upload_name = $_SESSION['teacher_id'].'_'.$_FILES['images']['name'][$i];

			// Προσωρινό όνομα αρχείου
			$source = $_FILES['images']['tmp_name'][$i]; 

			// Προορισμός αρχείου
			$dest = $photos_path.$upload_name;


			// Αν το αρχείο μεταφέρθηκε στον φάκελο
			// άλλαξε το πεδίο με το όνομά του στο _parent παράθυρο
			// αλλιώς εμφάνισε μύνημα βάσει του error number
			if (@move_uploaded_file($source,$dest)) 
			{ 
				chmod($dest, 0755);

				?>
				
				<SCRIPT>

				window.opener.document.getElementById('tmp').value += "<?php echo $upload_name; ?>\r\n";
				window.opener.document.getElementById('data_images').value += "<?php echo $upload_name; ?>\r\n";
				
				</SCRIPT>
				
				<?php

			} 
			else 
			{ 
				if ($error_number == 2)
				{
					$error .=  '<span class="red1">Εικόνα '.($i+1).': '._MAXFILEBYTES.' '.$_POST['MAX_FILE_SIZE'].' bytes</span>';
				}
			}
		}
	}

	// Άλλαξε τα δικαιώματα ξανά σε 755
	chmod($photos_path, 0755);

	if (empty($error) && !empty($filename))
	{
		echo '<SCRIPT>window.close();</SCRIPT>';
	}
} 

?>

<HTML> 
<HEAD> 
<TITLE>Αποστολή Φωτογραφιών</TITLE> 
</HEAD>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">

<BODY bgcolor="#FFFFFF">

<?php

if (!is_numeric($_POST['counter']) || $_POST['counter'] == 0)
{
	echo '
	<FORM METHOD="POST" ACTION="uploadMultiplePhotos.php?fld='.$_GET['fld'].'">
	<table border="0" cellpadding="4" cellspacing="2">
	<tr>
		<td>'._TYPENUMBERIMAGES.'</td>
	</tr>
	<tr>
		<td>'._NUMBEREQUALSWORDS.'</td>
	</tr>
	<tr>
		<td class="blue1">'._IMAGEFORMAT.'</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><input type="text" id="counter" name="counter" value="0" style="width:20px;" maxlength="1">&nbsp;'._MAXNUMBER.'</td>
	</tr>
	<tr>
		<td align="center"><br><input type="submit" value="Συνέχεια"></td>
	</tr>
	</table>

	</FORM>';
}
else
{
	echo '<p>'._IMAGESFILENAMEDESC;

	?>

	<FORM METHOD="POST" ENCTYPE="multipart/form-data" ACTION="uploadMultiplePhotos.php?fld=<?php echo $_GET['fld']; ?>"> 

	<INPUT TYPE="HIDDEN" NAME="action" VALUE="1">
	<INPUT TYPE="HIDDEN" NAME="counter" VALUE="<?php echo $_POST['counter']; ?>">
	<INPUT type="hidden" NAME="MAX_FILE_SIZE" value="100000">
	
	<?php

	for ($i=1 ; $i <= $_POST['counter'] ; $i++)
	{
		echo '<INPUT TYPE="FILE" NAME="images[]" SIZE="30"><BR>';
	}

	?>

	<INPUT TYPE="SUBMIT" VALUE="Αποστολή">

	</FORM> 

	<?php

	if (isset($error) && !empty($error))
	{
		echo $error;
	}
	else
	{
		echo _LINEWORDSEQUALSIMAGES;
	}
}

?>

</BODY> 

</HTML> 
<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Επεξεργασία Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL UPDATE RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'UPDATE antistoixisi SET limata = "'.trim($_POST['limata']).'", ermineies = "'.trim($_POST['ermineies']).'", viewable = '. $_POST['show_record'] .' WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


// SQL GET RECORDS /////////////////////////

$sql = 'SELECT limata, ermineies, viewable FROM antistoixisi WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];
$rs =& $dbconn->GetRow($sql);
////////////////////////////////////////////


echo '	<form id="mc" name="mc" method="post" action="edit_antistoixisi.php?id='.$_GET['id'].'">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._EDITANTISTOIXISH.'</td>
		</tr>
		</table>';

echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._WORDS.':</td>
			<td class="td2" width="255"><textarea id="limata" name="limata" style="width:250px;" rows="7" onKeyUp="checkTextAreaWordLength(\'limata\', \'\n\', 10, \''._ANTISTOIXISHMAXCHARSREACHED.'\');checkTextAreaLines(\'limata\', 4, \''._ANTISTOIXISHFAIL.'\')">'.$rs['limata'].'</textarea></td>
			<td class="td8">'._LIMATAWORDSDESC.'</td>
		</tr>
		<tr>
			<tr>
			<td class="td7" width="110">'._ERMINEIES.':</td>
			<td class="td2" width="255"><textarea id="ermineies" name="ermineies" style="width:250px;" rows="7" onKeyUp="checkTextAreaWordLength(\'ermineies\', \'\n\', 60, \''._ANTISTOIXISHMAXCHARSREACHED2.'\');checkTextAreaLines(\'ermineies\', 4, \''._ANTISTOIXISHERMINEIAFAIL.'\')">'.$rs['ermineies'].'</textarea></td>
			<td class="td8">'._ERMINEIESWORDSDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._SHOWINACTIVITY.':</td>
			<td class="td2" width="255">'.showInActivity($rs['viewable'],'').'</td>
			<td class="td8">'._SHOWINACTIVITYDESC.'</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>
<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Εισαγωγή Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL INSERT RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'INSERT INTO allazontas_protasi (teacher_id, sentence, accepted_sentences, viewable) VALUES ('.$_SESSION['teacher_id'].', "'.trim($_POST['sentence']).'", "'.trim($_POST['accepted_sentences']).'", '. $_POST['show_record'] .')';

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


echo '	<form id="mc" name="mc" method="post" action="insert_allazontas_protasi.php">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._INSERTALLAZONTASPROTASI.'</td>
		</tr>
		</table>';

echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._SENTENCES.':</td>
			<td class="td2" width="255"><textarea id="sentence" name="sentence" style="width:270px;" rows="7" onKeyUp="checkTextAreaWordsInLinesLength(\'sentence\', \';\', [9, 9, 9, 9], \''._ALLAZONTASPROTASICHARSREACHED.'\')"></textarea></td>
			<td class="td8">'._ALLAZONTASPROTASIDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._ALLACCEPTED.':</td>
			<td class="td2" width="255"><textarea id="sentence" name="accepted_sentences" style="width:270px;" rows="7"></textarea></td>
			<td class="td8">'._ALLACCEPTEDDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._SHOWINACTIVITY.':</td>
			<td class="td2" width="255">'.showInActivity(1,'').'</td>
			<td class="td8">'._SHOWINACTIVITYDESC.'</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>
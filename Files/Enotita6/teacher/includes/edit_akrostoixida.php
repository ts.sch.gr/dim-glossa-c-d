<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Επεξεργασία Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL UPDATE RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'UPDATE akrostoixida SET data_text = "'.trim($_POST['data_text']).'", data_ermineies = "'.trim($_POST['data_ermineies']).'", keimeno = "'.trim($_POST['keimeno']).'" WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


// SQL GET RECORDS /////////////////////////

$sql = 'SELECT data_text, data_ermineies, image, keimeno FROM akrostoixida WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];
$rs =& $dbconn->GetRow($sql);
////////////////////////////////////////////


echo '	<form id="mc" name="mc" method="post" action="edit_akrostoixida.php?id='.$_GET['id'].'">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._EDITAKROSTOIXIDA.'</td>
		</tr>
		</table>';


if (empty($rs['image']))
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('akrostoixida').'">'._INSERTIMAGE.'</a>';
}
else
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('akrostoixida').'">'._CHANGEIMAGE.'</a><br><a href="javascript:;" onclick="'.deleteImage().'">'._DELETEIMAGE.'</a>';
}


echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._WORDS.':</td>
			<td class="td2" width="255"><textarea id="data_text" name="data_text" style="width:250px;" rows="7" onKeyUp="checkTextAreaWordLength(\'data_text\', \'\n\', 10, \''._AKROSTOIXIDAFMSGWORDLENGTH.'\');checkTextAreaLines(\'data_text\', 10, \''._AKROSTOIXIDAFMSGWORDTOTAL.'\')">'.$rs['data_text'].'</textarea></td>
			<td class="td8">'._WORDSDESC3.'</td>
		</tr>
		<tr>
			<tr>
			<td class="td7" width="110">'._ERMINEIES.':</td>
			<td class="td2" width="255"><textarea id="data_ermineies" name="data_ermineies" style="width:250px;" rows="7" onKeyUp="checkTextAreaLines(\'data_ermineies\', 10, \''._AKROSTOIXIDAFMSGERMEINIESTOTAL.'\')">'.$rs['data_ermineies'].'</textarea></td>
			<td class="td8">'._WORDSDESC5.'</td>
		</tr>
		<tr>
		<!--tr>
			<td class="td7" width="110">'._IMAGE.':</td>
			<td class="td2" width="255">'.getImageForEI($rs['image'],'akrostoixida').'</td>
			<td class="td8">'.$image_link.'</td>
		</tr-->
		<tr>
			<tr>
			<td class="td7" width="110">'._KEIMENO.':</td>
			<td class="td2" width="255"><textarea id="keimeno" name="keimeno" maxlength="500" style="width:250px;" rows="7" onKeyUp="ismaxLength(this)">'.$rs['keimeno'].'</textarea></td>
			<td class="td8">'._WORDSDESC6.'</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>
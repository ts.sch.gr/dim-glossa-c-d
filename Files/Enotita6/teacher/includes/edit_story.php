<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Επεξεργασία Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL UPDATE RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'UPDATE story SET story_id = '.$_POST['story'].', rank = '.$_POST['rank'].', text = "'. $_POST['data_text'] .'", image = "'.$_POST['image'].'", sound = "'.$_POST['sound'].'" WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];

	$rs =& $dbconn->Execute($sql);
	echo $sql;
	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


// SQL GET RECORDS /////////////////////////

$sql = 'SELECT story_id, rank, text, image, sound FROM story WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];
$rs =& $dbconn->GetRow($sql);
////////////////////////////////////////////


echo '	<form id="mc" name="mc" method="post" action="edit_story.php?id='.$_GET['id'].'">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._EDITSTORY.'</td>
		</tr>
		</table>';


if (empty($rs['image']))
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('story').'">'._INSERTIMAGE.'</a>';
}
else
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('story').'">'._CHANGEIMAGE.'</a><br><a href="javascript:;" onclick="'.deleteImage().'">'._DELETEIMAGE.'</a>';
}

if (empty($rs['sound']))
{
	$sound_link = '<a href="javascript:;" onclick="'.uploadSoundLink('story').'">'._INSERTSOUND.'</a>';
}
else
{
	$sound_link = '<a href="javascript:;" onclick="'.uploadSoundLink('story').'">'._CHANGESOUND.'</a><br><a href="javascript:;" onclick="'.deleteSound().'">'._DELETESOUND.'</a>';
}


echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._STORY.':</td>
			<td class="td2" width="255"><input id="story" name="story" style="width:25px;" value="'.$rs['story_id'] .'" maxlength="3"></td>
			<td class="td8">'._STORYDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._RANK.':</td>
			<td class="td2" width="255"><input id="rank" name="rank" style="width:25px;" value="'. $rs['rank'] .'" maxlength="3"></td>
			<td class="td8">'._RANKDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._PARAGRAPH.':</td>
			<td class="td2" width="255"><textarea id="data_text" name="data_text" maxlength="130" style="width:250px;" rows="5" onKeyUp="ismaxLength(this)">'.$rs['text'].'</textarea></td>
			<td class="td8">'._PARAGRAPHDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._IMAGE.':</td>
			<td class="td2" width="255">'.getImageForEI($rs['image'],'story').'</td>
			<td class="td8">'.$image_link.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._SOUND.':</td>
			<td class="td2" width="255">'.getSoundForEI($rs['sound']).'</td>
			<td class="td8">'.$sound_link.'</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>
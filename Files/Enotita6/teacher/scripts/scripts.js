var subWindow = null;
var subWindow2 = null;

document.onkeydown = checkKey;

function checkKey(e)
{
	if (!e) var e = window.event;

	var oTarget = (e.target) ? e.target : e.srcElement;
	
	if (e.shiftKey)
	{
		// SHIFT + ~
		if (e.keyCode == 192)
		{
			if (!oTarget.type)
			{
				alert('ΕΡΓΑΣΤΗΡΙΟ ΤΟΥ ΔΑΣΚΑΛΟΥ\nTessera Multimedia S.A.');
			}
		}
	}
}

function disableItem(elem)
{
	if (document.forms[0].elements[elem])
	{
		document.forms[0].elements[elem].style.backgroundColor = "#DED9D0";
		document.forms[0].elements[elem].disabled = true;
	}
}

function enableItem(elem)
{
	if (document.forms[0].elements[elem])
	{
		document.forms[0].elements[elem].style.backgroundColor = "#ffffff";
		document.forms[0].elements[elem].disabled = false;
	}
}


function openWindow(file,width,height,top,left)
{
	var x = window.screen.width;
	var y = window.screen.height;

	var posx = (x-width)/2;
	var posy = (y-height)/2;
	
	subWindow2 = window.open(file,'','width='+width+',height='+height+',top='+posy+',left='+posx+',location=no,menubar=no,resizable=yes,scrollbars=no,status=yes,toolbar=no,fullscreen=no');
}

function popModal(file,width,height)
{	
	var x = window.screen.width;
	var y = window.screen.height;

	var posx = (x-width)/2;
	var posy = (y-height)/2;

	subWindow = window.open(file,'child','width='+width+',height='+height+',top='+posy+',left='+posx+',location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,fullscreen=no');
	subWindow.focus();
}

function checkLogin()
{
	if (document.getElementById('form_id').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το ID");
		return false;
	}
	else if (document.getElementById('form_pass').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το Password");
		return false;
	}
	else
	{
		return true;
	}
}

function checkRegistration()
{
	if (document.getElementById('lastname').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το επώνυμό σας");
		return false;
	}
	else if (document.getElementById('firstname').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το όνομά σας");
		return false;
	}
	else if (document.getElementById('email').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το email σας");
		return false;
	}
	else if (document.getElementById('school').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το σχολείο σας");
		return false;
	}
	else if (document.getElementById('password1').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε τον κωδικό");
		return false;
	}
	else if (document.getElementById('password2').value.length == 0)
	{
		alert("Παρακαλώ επαληθεύστε τον κωδικό");
		return false;
	}
	else if (document.getElementById('password1').value != document.getElementById('password2').value)
	{
		alert("Τα πεδία των κωδικών δεν ταιριάζουν μεταξύ τους");
		return false;
	}
	else
	{
		return true;
	}
}

function redirectTo(page)
{
	window.location = page;
}

function changeOrder(name)
{
	document.getElementById('order').value = name;
	document.forms[document.getElementById('order').form.id].submit();
}

function confirmDelete(id)
{
	var fRet; 
	
	fRet = confirm('Διαγραφή της εγγραφής ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('delete').value = id;
		document.forms[document.getElementById('delete').form.id].submit();
	}
}

function changeState(elemID)
{
	document.getElementById(elemID).value = true;
}

function changeColor(line,color)
{
	//document.getElementById(line).style.backgroundColor = color;
	return;
}

function changeImageDimensions(img_id)
{
	var w = document.getElementById(img_id).width;

	//alert(w);

	if (w != 0)
	{
		var percent = w/100;

		if (percent > 1)
		{
			document.getElementById(img_id).width = document.getElementById(img_id).width / percent;
		}

		document.getElementById(img_id).style.visibility = "visible";
	}
	else 
	{
		document.getElementById(img_id).style.visibility = "hidden";
		window.setTimeout("changeImageDimensions('"+img_id+"')", 100);
	}
}


//Ελέγχει το textObj, που χωρίζεται με separator. Η ιδιότητα text δε θα πρέπει να είναι 
//μεγαλύτερη maxConstraints. Αν συμβαίνει κάτι τέτοιο κάνε alert fmsg
function checkTextAreaWordsInLinesLength(textObj, separator, maxConstraints, fmsg) 
{
	lines = new Array();
	
	lineSeparator = isInternetExplorer()?"\r\n":"\n";
	lines = document.getElementById(textObj).value.split(lineSeparator);


	newFtext = "";
	modification = false;

	for (i=0 ; i < lines.length ; i++)
	{
		newText = "";
		words = new Array();
		words = lines[i].split(separator);

		for (j=0;j<words.length ;j++)
		{
			if (typeof(maxConstraints) == "number") 
			{
				maxLength = maxConstraints;
			}
			else 
			{
				if (maxConstraints[j] == undefined) 
				{
					maxLength = 3;
				}
				else 
				{
					maxLength = maxConstraints[j];
				}
			}
		
			if (words[j].length > maxLength)
			{
				words[j] = words[j].substring(0, maxLength);
				modification = true;
				alert(fmsg);
			}

			newText += words[j] + separator;
		}

		newFtext =  newFtext + newText.substring(0, newText.length - separator.length) + lineSeparator;

	}

	if (modification == true)
	{
		document.getElementById(textObj).value = newFtext.substring(0, newFtext.length - lineSeparator.length);
	}
}

function checkTextAreaWordLength(textObj, separator, maxLength, fmsg) 
{
	words = new Array();
	
	if (separator == '\n')
	{
		separator = isInternetExplorer()?"\r\n":separator;
	}

	words = document.getElementById(textObj).value.split(separator);


	newText = "";
	modification = false;

	for (i=0 ; i < words.length ; i++)
	{

		if (words[i].length > maxLength)
		{
			words[i] = words[i].substring(0, maxLength);
			modification = true;
			alert(fmsg);
		}
		newText += words[i] + separator;
	}

	if (modification == true)
	{
		document.getElementById(textObj).value = newText.substring(0, newText.length - separator.length);
	}
}


function checkTextAreaLines(textObj, lineLimit, fmsg) 
{
	lines = count('\n', document.getElementById(textObj).value);
	
	charsToCut = isInternetExplorer()?2:1;

	if (lines == lineLimit)
	{
		str = document.getElementById(textObj).value;
		document.getElementById(textObj).value = str.substring(0, str.length - charsToCut);
		
		//alert(fmsg);
	}

}


function checkInputTextLength(textObj, separator, maxLength, fmsg) 
{
	txt = document.getElementById(textObj).value;
	
	words = new Array();

	words = txt.split(separator);

	boxLength = 0;

	for (var i=0;i < words.length ;i++)
	{
		boxLength += words[i].length;
	}

	if (boxLength > maxLength)
	{
		document.getElementById(textObj).value = txt.substring(0, txt.length - (boxLength - maxLength));
		alert(fmsg);
	}

}


function count(what, where) 
{
	var _c = 0; // count
	
	//alert('Length: '+where.length + " text: " + where);
	
	for (var i=0 ; i < where.length ; i++) 
	{
		//alert(what + " " + where.substr(i, what.length));
		if (what == where.substr(i, what.length))
		{
			_c++;
		}
	}

	return _c;
}


function ismaxLength(obj)
{
	var mlength = obj.getAttribute? parseInt(obj.getAttribute("maxlength")) : ""

	if (obj.getAttribute && obj.value.length>mlength)
	{
		obj.value = obj.value.substring(0,mlength)
	}

}

function isInternetExplorer() 
{
	var agt = navigator.userAgent.toLowerCase();

	var is_ie = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
	return is_ie;
}

function deleteImage()
{
	var fRet; 
	
	fRet = confirm('Διαγραφή της Εικόνας ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('dummy').src = '../images/blank.gif';
		document.getElementById('image').value = '';
	}
}

function deleteSound()
{
	var fRet; 
	
	fRet = confirm('Διαγραφή του Ήχου ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('empty').value = '';
		document.getElementById('sound').value = '';
	}
}

function deleteMultipleImages()
{
	var fRet; 
	
	fRet = confirm('Διαγραφή Όλων των Εικόνων ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('tmp').value = '';
		document.getElementById('data_images').value = '';
	}
}
-- 
-- Βάση: `ergasthrio_gd`
-- 

-- 
-- Δομή Πίνακα για τον Πίνακα `activities`
-- 

DROP TABLE IF EXISTS `activities`;
CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(3) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `activity_table` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
)TYPE=MyISAM;

INSERT INTO `activities` VALUES (1, 'Ιστορία', 'story');
INSERT INTO `activities` VALUES (2, 'Κρυπτόλεξο', 'kryptolekso');
INSERT INTO `activities` VALUES (3, 'Παιχνίδι με τις λέξεις', 'maimou');
INSERT INTO `activities` VALUES (4, 'Ακροστιχίδα', 'akrostoixida');
INSERT INTO `activities` VALUES (5, 'Αντιστοίχιση', 'antistoixisi');
INSERT INTO `activities` VALUES (6, 'Αλλάζοντας τις λέξεις', 'allazontas_lekseis');
INSERT INTO `activities` VALUES (7, 'Συμπλήρωση κενών', 'simplirosh_kenon');
INSERT INTO `activities` VALUES (8, 'Αλλάζοντας την πρόταση', 'allazontas_protasi');
INSERT INTO `activities` VALUES (9, 'Φτιάχνω προτάσεις', 'ftiaxno_protaseis');

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `akrostoixida`
-- 

DROP TABLE IF EXISTS `akrostoixida`;
CREATE TABLE IF NOT EXISTS `akrostoixida` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` varchar(6) NOT NULL default '',
  `data_text` text NOT NULL,
  `data_ermineies` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  `keimeno` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `allazontas_lekseis`
-- 

DROP TABLE IF EXISTS `allazontas_lekseis`;
CREATE TABLE IF NOT EXISTS `allazontas_lekseis` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `word` text NOT NULL,
  `viewable` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `allazontas_protasi`
-- 

DROP TABLE IF EXISTS `allazontas_protasi`;
CREATE TABLE IF NOT EXISTS `allazontas_protasi` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `sentence` text NOT NULL,
  `accepted_sentences` text NOT NULL,
  `viewable` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `antistoixisi`
-- 

DROP TABLE IF EXISTS `antistoixisi`;
CREATE TABLE IF NOT EXISTS `antistoixisi` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `limata` text NOT NULL,
  `ermineies` text NOT NULL,
  `viewable` smallint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `ftiaxno_protaseis`
-- 

DROP TABLE IF EXISTS `ftiaxno_protaseis`;
CREATE TABLE IF NOT EXISTS `ftiaxno_protaseis` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `lekseis` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  `sound` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `kryptolekso`
-- 

DROP TABLE IF EXISTS `kryptolekso`;
CREATE TABLE IF NOT EXISTS `kryptolekso` (
  `id` int(6) NOT NULL auto_increment,
  `puzzle_id` int(6) NOT NULL default '0',
  `teacher_id` int(6) NOT NULL default '0',
  `data_text` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  `viewable` varchar(4) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `maimou`
-- 

DROP TABLE IF EXISTS `maimou`;
CREATE TABLE IF NOT EXISTS `maimou` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` varchar(6) NOT NULL default '',
  `leksi` varchar(20) NOT NULL default '',
  `ermineia` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `simplirosh_kenon`
-- 

DROP TABLE IF EXISTS `simplirosh_kenon`;
CREATE TABLE IF NOT EXISTS `simplirosh_kenon` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `word_without` text NOT NULL,
  `syllables` text NOT NULL,
  `show_record` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `story`
-- 

DROP TABLE IF EXISTS `story`;
CREATE TABLE IF NOT EXISTS `story` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `story_id` int(6) NOT NULL default '0',
  `rank` int(1) NOT NULL default '0',
  `text` text NOT NULL,
  `image` varchar(40) NOT NULL default '',
  `sound` varchar(40) NOT NULL default '',
  `viewable` varchar(4) NOT NULL default '',
  PRIMARY KEY  (`id`)
)TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `teachers`
-- 

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(6) NOT NULL auto_increment,
  `lastname` varchar(100) NOT NULL default '',
  `firstname` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `school` varchar(100) NOT NULL default '',
  `password` varchar(11) NOT NULL default '',
  `reg_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`)
)TYPE=MyISAM;
